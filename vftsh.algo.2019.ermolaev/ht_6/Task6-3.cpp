#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using std::cout;
using std::cin;
using std::istringstream;
using std::getline;
using std::endl;
using std::string;
using std::vector;

class BinarySearch 
{
public:
	int Search(vector<int>& array, int element)
	{
		int left = 0, right = array.size() - 1;
		while (left <= right)
		{
			int middle = left + (right - left) / 2;
			if (element == array[middle])
				return middle;
			if (element > array[middle])
				left = middle + 1;
			else
				right = middle - 1;
		}
		return -1;
	}
};

int main()
{
	BinarySearch binarySearcher;
	vector<int> array;
	string s;
	getline(cin, s);
	istringstream input(s);
	int a;
	while (input >> a)
		array.push_back(a);
	int element;
	cin >> element;
	cout << binarySearcher.Search(array, element);
	return 0;
}