#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
using namespace std;

class BinarySearch 
{
public:
	int Search(int element, vector<int>& array)
	{
		sort(array.begin(), array.end());
		int left = 0, int right = array.size() - 1;
		while (left < right)
		{
			int middle = (right - left) / 2;
			if (element == array[middle])
				return middle;
			else if (element > array[middle])
				left = middle;
			else
				right = middle;
		}
		return -1;
	}
};

int main()
{
	BinarySearch BinarySearcher;
	vector<int> array;
	string s;
	getline(cin, s);
	istringstream input(s);
	int a;
	while (input >> a)
		array.push_back(a);
	int element;
	cin >> element;
	cout << BinarySearcher.Search(element, array);
	return 0;
}