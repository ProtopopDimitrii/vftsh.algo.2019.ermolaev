#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <sstream>
using namespace std;

class Queue
{
	vector<int> Stack1, Stack2;
public:
	void Push(int value)
	{
		Stack1.push_back(value);
	}

	void Pop()
	{
		for (int i = 1; i < Stack1.size(); ++i)
			Stack2.push_back(Stack1[i]);
		Stack1.clear();
		for (int i = 0; i < Stack2.size(); ++i)
			Stack1.push_back(Stack2[i]);
		Stack2.clear();
	}

	int Start() {
		return Stack1[0];
	}
};

int main()
{
	Queue queue;
	string s;
	getline(cin, s);
	istringstream input(s);
	int a;
	while (input >> a)
		queue.Push(a);
	return 0;
}