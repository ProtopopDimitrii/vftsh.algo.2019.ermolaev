#include <iostream>
#include <string>
#include <sstream>
using std::cout; 
using std::cin;
using std::istringstream; 
using std::getline;
using std::endl;
using std::string;

struct StackElement
{
	int Value;
	int Minimum;
	StackElement* Next;
	StackElement(int value) 
	{
		Value = value;
	}
};

class StackWithMin
{
	StackElement* Head;
	int Size = 0;
public:
	void PushBack(int value)
	{
		StackElement* next = new StackElement(value);
		next->Minimum = Size == 0 ? value : Min(value, Head->Minimum);
		next->Next = Head;
		Head = next;
		++Size;
	}

	int Minimum()
	{
		return Head->Minimum;
	}

	void Pop()
	{
		if (Size > 0)
		{
			Head = Head->Next;
			--Size;
		}
	}

	int Top()
	{
		return Head->Value;
	}

private:
	int Min(int a, int b)
	{
		return a < b ? a : b;
	}

};

int main()
{
	StackWithMin stack;
	string s;
	getline(cin, s);
	istringstream input(s);
	int a;
	while (input >> a)
		stack.PushBack(a);
	cout << stack.Minimum() << endl;
	stack.Pop();
	cout << stack.Minimum() << endl;
	return 0;
}