#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::stringstream;
using std::multimap;
using std::endl;
using std::pair;

class DataBase
{
	multimap<string, string> People;

public: 
	void Add(string surname, string name)
	{
		People.insert(pair<string, string> (surname, name));
	}

	bool Check(string surname, string name)
	{
		if (People.count(surname) == 0)
			return false;
		auto range = People.equal_range(surname);
		auto start = range.first;
		auto end = range.second;
		while (start != end)
		{
			pair<string, string> human = *start;
			if (human.second == name)
				return true;
			++start;
		}
		return false;
	}

	void Delete(string surname, string name)
	{
		auto range = People.equal_range(surname);
		auto start = range.first;
		auto end = range.second;
		while (start != end)
		{
			pair<string, string> human = *start;
			if (human.second == name)
			{
				People.erase(start);
				return;
			}
			++start;
		}
	}

	vector<pair<string, string>> Show()
	{
		vector<pair<string, string>> humans;
		auto iter = People.begin();
		while (iter != People.end())
		{
			humans.push_back(*iter);
			++iter;
		}
		return humans;
	}

	vector<pair<string, string>> Select(string surname)
	{
		vector<pair<string, string>> humans;
		auto range = People.equal_range(surname);
		auto start = range.first;
		auto end = range.second;
		while (start != end)
		{
			humans.push_back(*start);
			++start;
		}
		return humans;
	}
};

int main()
{
	DataBase database;
	while (true)
	{
		string message;
		getline(cin, message);
		if (message == "add")
		{
			string surname, name;
			cin >> surname >> name;
			database.Add(surname, name);
		}
		else if (message == "check")
		{
			string surname, name;
			cin >> surname >> name;
			cout << database.Check(surname, name) ? "True" : "False";
			cout << endl;
		}
		else if (message == "delete")
		{
			string surname, name;
			cin >> surname >> name;
			database.Delete(surname, name);
		}
		else if (message == "show")
		{
			vector<pair<string, string>> people = database.Show();
			for (int i = 0; i < people.size(); ++i)
				cout << people[i].first << ' ' << people[i].second << endl;
		}
		else if (message == "select")
		{
			string surname;
			cin >> surname;
			vector<pair<string, string>> people = database.Select(surname);
			for (int i = 0; i < people.size(); ++i)
				cout << people[i].first << ' ' << people[i].second << endl;
		}
		else if (message == "stop")
			break;
	}
	return 0;
}