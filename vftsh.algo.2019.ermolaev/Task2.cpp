#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::stringstream;
using std::map;
using std::endl;
using std::pair;

int main()
{
	string text;
	vector<string> parsedText;
	getline(cin, text);
	stringstream stream(text);
	string buffer;
	while (stream >> buffer)
		parsedText.push_back(buffer);
	map<string, int> countOfWords;
	for (int i = 0; i < parsedText.size(); ++i)
		countOfWords[parsedText[i]]++;
	map<string, int>::iterator iter = countOfWords.begin();
	while (iter != countOfWords.end())
	{
		pair<string, int> word = *iter;
		cout << word.first << ": " << word.second << endl;
		++iter;
	}
	return 0;
}