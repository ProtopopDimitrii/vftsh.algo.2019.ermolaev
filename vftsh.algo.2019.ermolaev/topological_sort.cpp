#include <iostream>
#include <vector>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
using std::cin;
using std::cout;
using std::unordered_map;
using std::unordered_set;
using std::vector;
using std::stringstream;
using std::string;
using std::endl;

class TopologicalSort
{
	vector<int> Answer;
	vector<bool> Visited;
	unordered_map<int, vector<int>> Graph;
	int VertexCount;
public:
	TopologicalSort(unordered_map<int, vector<int>> graph)
	{
		Graph = graph;
		VertexCount = graph.size();
		Visited = vector<bool>(VertexCount + 1, false);
	}

	vector<int> Sort()
	{
		for (auto vertex = Graph.begin(); vertex != Graph.end(); ++vertex)
		{
			int name = (*vertex).first;
			if (!Visited[name])
				DFS(name);
		}
		return Answer;
	}

private:
	void DFS(int name)
	{
		Visited[name] = true;
		vector<int> edges = Graph[name];
		for (int i : edges)
			if (!Visited[i])
				DFS(i);
		Answer.push_back(name);
	}
};

int main()
{
	unordered_map<int, vector<int>> graph;
	vector<int> answer;
	int vertexCount;
	cin >> vertexCount;
	for (int i = 0; i < vertexCount; ++i)
	{
		int vertexName;
		cin >> vertexName;
		string line;
		getline(cin, line);
		stringstream stream(line);
		vector<int> outputVertexes;
		int vertex;
		while (stream >> vertex)
			outputVertexes.push_back(vertex);
		graph[vertexName] = outputVertexes;
	}
	TopologicalSort sort(graph);
	answer = sort.Sort();
	for (int i = answer.size() - 1; i >= 0; --i)
		cout << answer[i];
	return 0;
}