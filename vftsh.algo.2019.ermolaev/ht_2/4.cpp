#include <iostream>
#include <queue>
#include <string>
using namespace std;

int parseInt(string order)
{
    string &order1 = order;
    int result = 0;
    int sign = 1;
    for(int i = 0; i < order.length(); i++)
    {
        if(order[i] >= '0' && order[i] <= '9')
            result = result * 10 + (order[i] - '0');
        else if(order[i] == '-' && result == 0)
            sign = -1;
        else if(order[i] == '-' && result >= 0)
            break;
        else if(order[i] == '+' || order[i] == '/' || order[i] == '*')
            break;
        else if(order[i] == ' ')
            continue;
    }
    result *= sign;
    return result;
}

int main()
{
    string order;
    queue<int> Order;
    int result = 0;
    getline(cin, order);
    for(int i = 0; i < order.length(); i++)
    {
        if(order[i] >= '0' && order[i] <= '9')
            result = 10 * result + (order[i] - '0');
        else
        {
            Order.push(result);
            result = 0;
        }
    }
    Order.push(result);
    result = 0;
    while(Order.size() > 1)
    {
        int digit1 = Order.front();
        Order.pop();
        int digit2 = Order.front();
        Order.pop();
        cout << digit1 << ' ' << digit2 << endl;
        if((digit1 + digit2) % 2 == 0)
            Order.push(digit1 + digit2);
        else
            Order.push(digit1 - digit2);
    }
    cout << Order.front();
    return 0;
}