#include <iostream>
using std::cout;
using std::endl;

struct TStackEl 
{
	int Data;
	TStackEl* Next;
};

class TStack 
{
	unsigned int Len;
	TStackEl* Head;
public:
	TStack() : Len(0) {}

	void Pop() 
	{
		if (Len > 0) 
		{
			TStackEl* newHead = Head->Next;
			delete Head;
			Head = newHead;
			--Len;
		}
	}

	int Top() 
	{
		return Head->Data;
	}

	void Push(int x) 
	{
		TStackEl* newEl = new TStackEl;
		newEl->Data = x;
		newEl->Next = Head;
		Head = newEl;
		++Len;
	}

	bool Empty() 
	{
		return Len == 0;
	}

	unsigned int Size() 
	{
		return Len;
	}
};

int main()
{
	TStack stack;
	stack.Push(0);
	stack.Push(1);
	cout << stack.Top() << ' ' << stack.Size() << endl;
	stack.Pop();
	cout << stack.Top() << ' ' << stack.Size() << endl;
	stack.Pop();
	cout << stack.Top() << ' ' << stack.Size() << endl;
	stack.Push(2);
	cout << stack.Top() << ' ' << stack.Size() << endl;
	return 0;
}