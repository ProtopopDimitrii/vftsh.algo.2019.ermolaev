#include <iostream>
#include <stack>
using std::cout; using std::cin; using std::string; using std::stack;

int main()
{
    string order;
    cin >> order;
    stack<char> Stack;
    for(int i = 0; i < order.length(); i++)
    {
        if(order[i] == '{' || order[i] == '(' || order[i] == '[')
            Stack.push(order[i]);
        else if(order[i] == ')' && !Stack.empty() && Stack.top() == '(')
            Stack.pop();
        else if(order[i] == '}' && !Stack.empty() &&  Stack.top() == '{')
            Stack.pop();
        else if(order[i] == ']' && !Stack.empty() && Stack.top() == '[')
            Stack.pop();
        else
        {
            cout << "Wrong";
            return 0;
        }    
    }
    if(Stack.empty())
        cout << "Right";
    else
        cout << "Wrong";
    return 0;
}