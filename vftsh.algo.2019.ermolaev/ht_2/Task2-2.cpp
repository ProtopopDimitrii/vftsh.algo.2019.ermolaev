#include <iostream>
using std::cout;
using std::cin;

struct TDequeElement
{
	TDequeElement* Previous;
	TDequeElement* Next;
	int Value;
};

class TDeque
{
	TDequeElement* Front;
	TDequeElement* Back;
	unsigned int Length = 0;
public:
	void PushBack(int value)
	{
		TDequeElement* newElem = new TDequeElement();
		newElem->Value = value;
		newElem->Previous = Back;
		Back->Next = newElem;
		Back = newElem;
		++Length;
	}

	void PushFront(int value)
	{
		TDequeElement* newElem = new TDequeElement();
		newElem->Value = value;
		newElem->Next = Front;
		Front->Previous = newElem;
		Front = newElem;
		++Length;
	}

	void PopBack()
	{
		if (Length > 0)
		{
			TDequeElement* newBack = Back->Previous;
			delete Back;
			Back = newBack;
			--Length;
		}
	}

	void PopFront()
	{
		if (Length > 0)
		{
			TDequeElement* newFront = Front->Next;
			delete Front;
			Front = newFront;
			--Length;
		}
	}

	bool IsEmpty()
	{
		return Length == 0;
	}

	int Size()
	{
		return Length;
	}

	int Top()
	{
		return Back->Value;
	}

	int Start()
	{
		return Front->Value;
	}
};

int main()
{
	TDeque deque;
	deque.PushBack(1);
	deque.PushBack(2);
	deque.PushBack(3);
	deque.PushFront(4);
	deque.PushFront(5);
	deque.PushFront(6);
	deque.PopBack();
	cout << deque.Top();
	deque.PopBack();
	cout << deque.Top();
	deque.PopBack();
	cout << deque.Top();
	deque.PopFront();
	cout << deque.Start();
	return 0;
}