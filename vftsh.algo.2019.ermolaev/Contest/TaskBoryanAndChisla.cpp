#include <iostream>
#include <deque>
#include <sstream>
using std::deque;
using std::string;
using std::istringstream;
using std::getline;
using std::cin;
using std::cout;

int main()
{
	deque<int> Digits;
	string s;
	getline(cin, s);
	getline(cin, s);
	istringstream input(s);
	int digit;
	while (input >> digit)
		Digits.push_back(digit);
	while (Digits.size() > 1)
	{
		int a = Digits[0];
		int b = Digits[1];
		Digits.pop_front();
		Digits.pop_front();
		if ((a + b) % 2 == 0)
			Digits.push_back(a + b);
		else
			Digits.push_back(a - b);
	}
	cout << Digits[0];
	return 0;
}