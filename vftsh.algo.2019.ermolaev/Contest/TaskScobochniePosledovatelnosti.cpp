#include <iostream>
#include <stack>
using std::cout; 
using std::cin; 
using std::string;
using std::stack;

int main()
{
	string order;
	cin >> order;
	stack<char> charOrder;
	for (int i = 0; i < order.length(); ++i)
	{
		if (order[i] == '{' || order[i] == '(' || order[i] == '[')
			charOrder.push(order[i]);
		else if (order[i] == ')' && !charOrder.empty() && charOrder.top() == '(')
			charOrder.pop();
		else if (order[i] == '}' && !charOrder.empty() && charOrder.top() == '{')
			charOrder.pop();
		else if (order[i] == ']' && !charOrder.empty() && charOrder.top() == '[')
			charOrder.pop();
		else
		{
			cout << "Incorrect";
			return 0;
		}
	}
	if (charOrder.empty())
		cout << "Correct";
	else
		cout << "Incorrect";
	return 0;
}