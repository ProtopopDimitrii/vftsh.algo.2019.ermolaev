#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void qsort(vector<int>& array, int left, int right)
{
    if (left >= right)
        return;
    int pivotIndex = left + (right - left) / 2;
    int pivot = array[pivotIndex];
    int i = left, j = right;
    while (i <= j)
    {
        while (array[i] < pivot)
            ++i;
        while (array[j] > pivot)
            --j;
        if (i <= j) 
        {
            int x = array[i];
            array[i] = array[j];
            array[j] = x;
            ++i; --j;
        }
    }
    if (left < j)
        qsort(array, left, j);
    if (right > i)
        qsort(array, i, right);
}

int main()
{
    vector<int> arr;
    string s;
    getline(cin, s);
    istringstream input(s);
    int a;
    while (input >> a)
        arr.push_back(a);
    qsort(arr, 0, arr.size());
    for(int i = 0; i < arr.size(); ++i)
        cout << arr[i] << ' ';
    return 0;
}